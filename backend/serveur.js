const express = require('express')
const bodyParser = require('body-parser');
const cors = require('cors');

const app = express();
const port = 3000;

let boissons = [];
let evaluations = [];
let currentId = 0;
let currentIdEval = 0;

// Configuring body parser middleware
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
app.use(cors());
app.post('/boisson', (req, res) => {
    const name = req.body.name;
    currentId++;
    const id = currentId;
    boissons.push({id:id,name:name});
});

app.get('/boissons', (req, res) => {
    res.json(boissons);
});

app.get('/boisson/:id', (req, res) => {
    const id = req.params.id;
    for (let boisson of boissons) {
        if (boisson.id === id) {
            res.json(boisson);
            return;
        }
    }

    // Sending 404 when not found something is a good practice
    res.status(404).send('Boisson not found');
});

app.delete('/boisson/:id', (req, res) => {
    const id = req.params.id;
    boissons = boissons.filter(i => {
        if (i.id != id) {
            return true;
        }
        return false;
    });
    res.send('Boisson is deleted');
});
app.post('/boisson/:id', (req, res) => {
  const id = req.params.id;
  const name = req.body.name;

  for (let i = 0; i < boissons.length; i++) {
      let boisson = boissons[i];
      if (boisson.id == id) {
          boissons[i].name = name;
      }
  }
});

//evaluations
app.get('/moyennes', (req, res) => {
    const boissonID = req.params.idBoisson;
    const currentEvals = [];
    for (let i = 0; i < evaluations.length; i++) {
      let eval = evaluations[i];
      if (eval.idBoisson == boissonID) {
        currentEvals.push(eval);
      }
    }
    const tableAvg = [];
    let total = 0;
    for (let i = 0; i < currentEvals.length; i++) {
      total+= currentEvals[i].note;
    }
    let avg = total/currentEvals.length;
    res.json({avg:avg});
});
app.get('/moyenne/:idBoisson', (req, res) => {
    const boissonID = req.params.idBoisson;
    const currentEvals = [];
    for (let i = 0; i < evaluations.length; i++) {
      let eval = evaluations[i];
      if (eval.idBoisson == boissonID) {
        currentEvals.push(eval);
      }
    }
    let total = 0;
    for (let i = 0; i < currentEvals.length; i++) {
      total+= currentEvals[i].note;
    }
    let avg = total/currentEvals.length;
    res.json({avg:avg});
});
app.post('/evaluation', (req, res) => {
	console.log("test");
    const name = req.body;
    currentIdEval++;
	console.log(name);
    evaluations.push({id: currentIdEval, idBoisson: name.idBoisson, note: name.note, commentaire: name.commentaire, lieu: name.lieu,
			auteur: name.auteur, date: name.date});
	console.log(evaluations);
});
app.get('/evaluations/:idBoisson', (req, res) => {
    const boissonID = req.params.idBoisson;
    const currentEvals = [];
    for (let i = 0; i < evaluations.length; i++) {
      let eval = evaluations[i];
      if (eval.idBoisson == boissonID) {
        currentEvals.push(eval);
      }
    }
    res.json(currentEvals);
});
app.get('/evaluations', (req, res) => {
    res.json(evaluations);
});

app.get('/evaluation/:id', (req, res) => {
    // Reading isbn from the URL
    const id = req.params.id;
    // Searching books for the isbn
    for (let evaluation of evaluations) {
        if (evaluation.id === id) {
            res.json(evaluation);
            return;
        }
    }

    // Sending 404 when not found something is a good practice
    res.status(404).send('Evaluation not found');
});

app.delete('/evaluation/:id', (req, res) => {
    // Reading isbn from the URL
    const id = req.params.id;

    // Remove item from the books array
    evaluations = evaluations.filter(i => {
        if (i.id !== id) {
            return true;
        }
        return false;
    });
    res.send('Evaluation is deleted');
});
app.post('/evaluation/:id', (req, res) => {
    // Reading isbn from the URL
    const id = req.params.id;
    const newEvaluation = req.body;

    // Remove item from the books array
    for (let i = 0; i < evaluations.length; i++) {
        let evaluation = evaluations[i]
        if (evaluation.id === id) {
            evaluations[i] = newEvaluation;
        }
    }

    res.send('Evaluation is edited');
});



app.listen(port, () => console.log(`Servuer app listening on port ${port}!`));
