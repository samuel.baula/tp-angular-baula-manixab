import { Boisson } from '../boisson';
import { Component, OnInit, Input, ViewChild } from '@angular/core';

import { HttpClient } from '@angular/common/http';
import { HttpHeaders } from '@angular/common/http';

import { EvaluationsComponent } from '../evaluations/evaluations.component'
@Component({
  selector: 'app-boisson-detail',
  templateUrl: './boisson-detail.component.html',
  styleUrls: ['./boisson-detail.component.css']
})
export class BoissonDetailComponent implements OnInit {
  @Input() boisson: Boisson;
  @ViewChild(EvaluationsComponent, { static: false }) evals: EvaluationsComponent;
  @Input() refreshList: () => {};
  constructor(private http: HttpClient) { }
  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization': 'my-auth-token'
    })
  }
  ngOnInit() {
  }
  refreshEvals() {
    console.log('refresh evals');
    if (this.evals != undefined) {
        this.evals.getEvaluations();
    }
  }
  delete(boissonID) {
    console.log('DELETE');
    console.log(boissonID);
    this.http
      .delete('http://localhost:3000/boisson/' + boissonID)
      .subscribe(response => {
        console.log(response);
      });
    //this.refreshList();
    location.reload();
    // Pas réussi à faire avec la promise ou autre pour attendre le resultat
  }
  edit(boissonID) {
    this.http
      .post('http://localhost:3000/boisson/' + boissonID, JSON.stringify({ name: this.boisson.name }), this.httpOptions)
      .subscribe(response => {
        console.log(response);
      });
    this.refreshList();
  }
}
