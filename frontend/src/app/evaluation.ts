export class Evaluation{
  id: number;
  idBoisson: number;
  note: number;
  commentaire: string;
  lieu: string;
  auteur: string;
  date: string;
}
