import { Injectable } from '@angular/core';

import { Observable, of } from 'rxjs';

import { Evaluation } from './evaluation';
import { EVALUATIONS } from './evaluations';
@Injectable({
  providedIn: 'root'
})
export class EvaluationsService {
  currentEvaluations: Evaluation[];
  constructor() { }
  getEvaluations(number): Observable<Evaluation[]> {
    this.currentEvaluations = [];
    for (let a = 0; a < EVALUATIONS.length; a++) {
      if (EVALUATIONS[a].idBoisson == number) {
        this.currentEvaluations.push(EVALUATIONS[a]);
      }
    }
    return of(this.currentEvaluations);
  }
}
