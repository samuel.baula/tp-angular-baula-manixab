import { Component, OnInit, Input } from '@angular/core';
import { Evaluation } from '../evaluation';
import { EvaluationsService } from '../evaluations.service';
import { FormBuilder, FormGroup } from "@angular/forms";
import { HttpClient } from '@angular/common/http';
import { HttpHeaders } from '@angular/common/http';
@Component({
  selector: 'app-evaluations',
  templateUrl: './evaluations.component.html',
  styleUrls: ['./evaluations.component.css']
})
export class EvaluationsComponent implements OnInit {
  @Input() idBoisson: number;
  evaluations: Evaluation[];
  eval: Evaluation;
  formEval: FormGroup;
  isAdd: boolean = false;
  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization': 'my-auth-token'
    })
  }
  //constructor(private evaluationsService: EvaluationsService) { }
  constructor(private evaluationsService: EvaluationsService, private http: HttpClient, public fb: FormBuilder) {
  this.formEval = this.fb.group({
    note: [''],
    comment: [''],
    lieu: [''],
    author: [''],
    date: [''],
  })
  }

  ngOnInit() {
    this.getEvaluations();
  }

  getEvaluations(): void {
    this.http.get("http://localhost:3000/evaluations/"+ this.idBoisson)
                .subscribe((data : any[]) => {
                  this.evaluations = data;
                })
  }
  addEval(): void {
    this.isAdd = true;
  }

  addEvaluation() {
    console.log("test");
    console.log(this.formEval.value.note);
    this.http
      .post('http://localhost:3000/evaluation', JSON.stringify({
        idBoisson: this.idBoisson, note: this.formEval.value.note, commentaire: this.formEval.value.comment, lieu: this.formEval.value.lieu,
        auteur: this.formEval.value.author, date: this.formEval.value.date
      }), this.httpOptions)
      .subscribe(response => {
        console.log(response);
      });
      this.isAdd = false;
  }
}
