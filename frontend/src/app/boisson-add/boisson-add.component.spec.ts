import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BoissonAddComponent } from './boisson-add.component';

describe('BoissonAddComponent', () => {
  let component: BoissonAddComponent;
  let fixture: ComponentFixture<BoissonAddComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BoissonAddComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BoissonAddComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
