import { Component, OnInit, Input } from '@angular/core';
import { FormBuilder, FormGroup } from "@angular/forms";

import { HttpClient } from '@angular/common/http';
import { HttpHeaders } from '@angular/common/http';
@Component({
  selector: 'app-boisson-add',
  templateUrl: './boisson-add.component.html',
  styleUrls: ['./boisson-add.component.css']
})
export class BoissonAddComponent implements OnInit {
  form: FormGroup;
  @Input() isAdd: Boolean;
  @Input() refreshList: () => {};
  idCurrentBoisson: number;
  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization': 'my-auth-token'
    })
  }
  constructor(private http: HttpClient, public fb: FormBuilder) { this.idCurrentBoisson = 0, this.form = this.fb.group({
      name: ['']
    }) }

  ngOnInit() {
  }
  addBoisson() {
    console.log(this.form.value.name);
    this.http
      .post('http://localhost:3000/boisson', JSON.stringify({name: this.form.value.name}), this.httpOptions)
      .subscribe(response => {
        console.log(response);
      });
      location.reload();
      // Pas réussi à faire avec la promise ou autre pour attendre le resultat
      //this.isAdd = false;
      //this.refreshList();

  }
}
