import { Component, OnInit, ViewChild  } from '@angular/core';
import { Boisson } from '../boisson';

import { HttpClient } from '@angular/common/http';

import { BoissonDetailComponent } from '../boisson-detail/boisson-detail.component'
@Component({
  selector: 'app-boissons',
  templateUrl: './boissons.component.html',
  styleUrls: ['./boissons.component.css']
})
export class BoissonsComponent implements OnInit {
  selectedBoisson: Boisson;
  isAdd:boolean = false;
  lesBoissons: Boisson[] = [];
  average: [];
  @ViewChild(BoissonDetailComponent, {static: false}) details: BoissonDetailComponent ;
  constructor(private http: HttpClient) { }

  ngOnInit() {
    this.getBoissons();
  }
  onSelect(boisson: Boisson): void {
    this.selectedBoisson = boisson;
    this.details.refreshEvals();
  }
  getAvg(): void{
    console.log(this.lesBoissons);
    this.http.get("http://localhost:3000/moyenne/1")
                .subscribe((data : any[]) => {
                  this.lesBoissons = data;
                })
  }
  getBoissons(): void {
    this.http.get("http://localhost:3000/boissons")
                .subscribe((data : any[]) => {
                  this.lesBoissons = data;
                  console.log(this.lesBoissons);
                })
  }

  addBoisson(): void{
    this.isAdd = true;
    this.selectedBoisson = null;
  }
}
