import { TestBed } from '@angular/core/testing';

import { BoissonAddService } from './boisson-add.service';

describe('BoissonAddService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: BoissonAddService = TestBed.get(BoissonAddService);
    expect(service).toBeTruthy();
  });
});
