import { Boisson } from './boisson';
export const BOISSONS: Boisson[] = [
  { id: 1, name: 'Bière boje', details: 'Bière très boje à boire en intant' },
  { id: 2, name: 'Vin int 21', details: 'Vin très glissant super boje' },
  { id: 3, name: 'Thé Hasagi', details: 'Thé incurvé aux épices de run down' }
];
