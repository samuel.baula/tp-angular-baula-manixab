import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { ReactiveFormsModule } from '@angular/forms';

import { AppComponent } from './app.component';
import { BoissonsComponent } from './boissons/boissons.component';
import { BoissonDetailComponent } from './boisson-detail/boisson-detail.component';
import { EvaluationsComponent } from './evaluations/evaluations.component';



import { HttpClientModule } from '@angular/common/http';
import { BoissonAddComponent } from './boisson-add/boisson-add.component';
@NgModule({
  declarations: [
    AppComponent,
    BoissonsComponent,
    BoissonDetailComponent,
    EvaluationsComponent,
    BoissonAddComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpClientModule,
    ReactiveFormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
